var url="https://kim.kaizen.com/new/kim-home.html";
function loadMonth(year,month,next_prev)
{
	$("#overlay").show();
	reDrawCharts(true);
	var params = {year:year,month:month,isAjax:true,next_prev:next_prev};
	params.consultants=new Array($("#consultant-selector").val());
	var monthRequest = $.get(url,params);
	monthRequest.done(function(data){
		$(".calendar-container").replaceWith(data);
		showMobileInfo($(".today .mobile-day-info"));
	});
	monthRequest.always(function(){
		$("#overlay").hide();
	});
}
function showMobileInfo(element,event)
{
	var list_html="";
	if($(element).length)
	{
		var events_info = $(element).data("events-info");
		var day_consultant = $(element).data("day-consultant");
		var day_date = $(element).data("day-date");
		//list_html+="<li data-day-consultant='"+day_consultant+"' data-day-date='"+day_date+"'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span></li>";
		list_html+="<li data-day-consultant='"+day_consultant+"' data-day-date='"+day_date+"'></li>";
		for(var i=0;i<events_info.length;i++)
		{
			var from_time = "";
			if(events_info[i].from_time)
				from_time=events_info[i].from_time;
			else if(events_info[i].to_time)
				from_time="08:00";
			var to_time = "";
			if(events_info[i].to_time)
				to_time=events_info[i].to_time;
			else if(events_info[i].from_time)
				to_time="20:00";
			var time_info = from_time.length>0?from_time + " - " + to_time: events_info[i].n_days;
			list_html+="<li data-day-id='"+events_info[i].day_id+"' data-day-project-code='"+events_info[i].project_code+"' data-day-consultant='"+events_info[i].consultant+"' data-day-date='"+events_info[i].date+"' style='background-color:"+events_info[i].background_color+"'>"+time_info+" - "+events_info[i].project_code+"</li>";
		}
	}
	$("#mobile-area-info").html(list_html);
	if(event)
	{
		event.stopPropagation();
		event.preventDefault();
	}
	return false;
}
function showDayModal(elem,event)
{
	$("#modal-overlay").show();
	var iframe_url = "https://kim.kaizen.com/planner/planner_new.php?intrexx=1";
	var day_date = $(elem).data("day-date");
	var day_consultant = $(elem).data("day-consultant");
	iframe_url+="&da="+day_date+"&co="+day_consultant;
	if(typeof $(elem).data("day-id") && $(elem).data("day-id"))
	{
		var day_id = $(elem).data("day-id");
		iframe_url+="&id="+day_id;
	}
	$("#day-form").prop("src",iframe_url);
	$("#day-form").on("load",function(){$("#modal-overlay").hide();});
	var day_project_code = $(elem).data("day-project-code");
	//$('#day-modal-title').text(day_consultant+" - "+day_date+" - "+day_project_code);
	$('#day-modal').modal();
	if(event)
	{
		event.stopPropagation();
		event.preventDefault();
	}
	return false;
}

showMobileInfo($(".today .mobile-day-info"));
$("#outter-calendar").on("click","#mobile-area-info li",function(){
	showDayModal(this);
});

$('.charts-control').datepicker({
	format:"dd.mm.yyyy"
});

var days_data_global = null;
function drawChart(chartArea)
{
	var chart_type = $(chartArea).find(".chart").attr("id");
	google.charts.setOnLoadCallback(function(){
		var days = days_data_global[chart_type];
		var array_date = new Array(['Type','Days']);
		var slice_colors = {};
		var color_counter = 0;
		for(var day_label in days)
		{
			array_date.push([day_label,days[day_label].total_days]);
		}
		array_date.sort(function(a,b){
			if(a[1]==='Days')
				return -1;
			if(a[1]===b[1])
				return 0;
			return (a[1]>=b[1])?-1:1;
		});
		for(var i=1;i<array_date.length;i++)
		{
			if(days[array_date[i][0]].color)
				slice_colors[i-1]={color:days[array_date[i][0]].color};
		}
		var data = google.visualization.arrayToDataTable(
			array_date
		);
		var options = {
			title: 'Days per type',
			backgroundColor:'#e4e4e4',
			chartArea:{left:0,top:0,width:'100%',height:'90%'},
			slices:slice_colors,
			pieSliceTextStyle:{color:'black'},
			is3D: true,
			legend:{
				position:'labeled'
			}
		};
		var chart_holder = $(chartArea).find(".chart-holder").get(0);
		var chart = new google.visualization.PieChart(chart_holder);
        chart.draw(data, options);
	});
}
function loadChart(chartArea)
{
	$(chartArea).find(".refresh").show();
	var chart_type = $(chartArea).find(".chart").attr("id");
	var start_date = $(chartArea).find(".start_date").val();
	var end_date = $(chartArea).find(".end_date").val();
	var params = {start_date:start_date,end_date:end_date,isAjax:true,chart_type:chart_type};
	params.consultants=new Array($("#consultant-selector").val());
	var days_request = $.get(url,params);
	days_request.done(function(days_data){
		days_data_global = days_data;
		drawChart(chartArea);
	});
	days_request.always(function(){
		$(chartArea).find(".refresh").hide();
	});
}
google.charts.load('current', {'packages':['corechart']});


function reDrawCharts(ajax)
{
	var chart_areas = $(".chart-area");
	for(var i=0;i<chart_areas.length;i++)
	{
		if(typeof ajax!='undefined' && ajax)
			loadChart(chart_areas[i]);
		else	
			drawChart(chart_areas[i]);
	}
}

//redraw graph when window resize is completed  
$(window).on('resizeEnd', function() {
	reDrawCharts();
});
//create trigger to resizeEnd event     
$(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});

reDrawCharts(true);

$(".chart-area").on("change",".charts-control",function(event){
	loadChart(event.delegateTarget);
});
