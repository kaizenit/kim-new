<?php
$pages = $page_table->getRecorsivePages($lang, $current_page['lid']);

function printRecorsive($page_holder,$level=1,$alias_so_far="")
{
	$alias_so_far.="/{$page_holder['alias']}";
	?>
	<li class="main-nav__item<?= !empty($page_holder['children'])?" main-nav__item--has-child":"" ?>">
		<a style="<?= $level>1? "padding-left:".(($level-1)*10)."%":"" ?>" class="main-nav__link main-nav__link--parent <?= $level>1?"main-nav__link--child":""?>" href="<?= !isset($page_holder['shortcut'])?"{$alias_so_far}.html":$page_holder['shortcut'] ?>"><?= substr($page_holder['menu_title'],0,22) ?>
			<i class="main-nav__icon icon <?= !empty($page_holder['children'])?"icon-custom":"icon-chevron-right"; ?>"></i>
		</a>
		<?php if(!empty($page_holder['children'])){?>
		<div class="main-nav__secondary-nav">
			<div class="main-nav__secondary-content">
				<div class="clear">
					<ul class="main-nav__list--child">
					<?php foreach($page_holder['children'] as $page_children){
									if(!empty($page_children['children']))
										printRecorsive($page_children,$level+1,$alias_so_far);
									else
									{
					?>
										<li class="main-nav__item--child">
											<a class="main-nav__link main-nav__link--child clickable" style="padding-left:<?= $level*10 ?>%" href="<?= !isset($page_holder['shortcut'])?"{$alias_so_far}/{$page_children['alias']}.html":$page_holder['shortcut'] ?>" >
												<?= $page_children['menu_title'];?>
												<i class="main-nav__icon icon icon-chevron-right"></i>
											</a>
										</li>
					<?php 	}
								}
					?>
					</ul>
				</div>
			</div>
		</div>
		<?php 
		}?>
	</li>
<?php 
}


?>

<header class="main-header visible-xs visible-sm">
<nav class="main-nav">
<header class="main-nav__header main-header__title">
<a href="#">
<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
<span style="margin-left:6px">Contact</span>
</a>
<span class="main-nav_current-label"><?= $current_page['menu_title']?></span>
		</header>
		<ul class="main-nav__list clear">
			<?php 
			foreach($pages as $page_holder)
			{
				printRecorsive($page_holder);
			}
			?>
		</ul>
		<button class="main-nav__btn btn--toggle-nav">
			<i class="icon icon-menu">
			</i>
		</button>
	</nav>
</header>
<div class="container-fluid">
	<div class="row globe-language">
		<div class="col-xs-8 col-md-6 col-lg-8">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3">
					<a href="/">
						<img class="img-responsive" alt="Kaizen Institute Logo" title="Kaizen Institute Logo" src="https://kim.kaizen.com/kimglobal/userfiles/Image/gl/brand/kaizen-institute-corporate-logo/Kaizen_Institute_Logo_RGB.svg" />
					</a>
				</div>
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-6 col-lg-4">
			<div class="row">
				<div class="col-xs-6 text-right">
					<i class="glyphicon glyphicon-globe" style="font-size:1.5em;" aria-hidden="true"></i>
					<span style="position:relative;bottom:6px;padding-left:4px">Kaizen Institute</span>
				</div>
				<div class="col-xs-6 text-right" style="top:6px">
					<span style="padding-left:6px">English</span>
					<span style="padding-left:10px">Deutsch</span>
				</div>
			</div>
		</div>
	</div>
	<div class="row hidden-xs hidden-sm horizontal-menu">
		<div class="col-xs-12">
			<form class="form-inline has-feedback" style="float: right;position: relative;bottom: 8px;margin-left:10px">
				<input type="search" class="form-control" placeholder="Search" />
				<i class="glyphicon glyphicon-search form-control-feedback"></i>
			</form>
			<ul class="horizontal-nav" style="position:relative;">
				<?php
				foreach($pages as $page_holder):
				?>
					<li class="<?= !empty($page_holder['children'])?"dropdown":"" ?> menu-entry <?= $current_page['lid']==$page_holder['lid']?" active":""?>">
						<a <?php if(!empty($page_holder['children'])): ?> data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <?php endif; ?> class="big-menu-link <?php if(!empty($page_holder['children'])):?> dropdown-toggle <?php endif;?>" href="<?= "{$page_holder['alias']}.html" ?>"><?= $page_holder['menu_title']?></a>
						<?php if(!empty($page_holder['children'])):?>
						<ul class="dropdown-menu">
							<ul class="list-inline">
								<?php foreach($page_holder['children'] as $children):?>
									<li>
										<a href="#"><?= $children['menu_title']?></a
									</li>
								<?php endforeach;?>
							</ul>
            			</ul>
            			<?php endif;?>
					</li>
				<?php 
				endforeach;
				?>
			</ul>
		</div>
	</div>