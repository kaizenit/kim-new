<?php
/**
 *
 * @param string $alias alias to be transalted into table column
 * @param array $translater table translator
 */
function getColumnFromAlias($alias,$translater)
{
	$column_name = null;
	foreach($translater as $prefix=>$table_translater_data)
	{
		foreach($table_translater_data as $translater_data)
		{
			if(strtolower($translater_data['alias'])==strtolower(trim($alias)))
			{
				$column_name=$translater_data['column'];
				break 2;
			}
		}
	}
	if(!isset($column_name))
	{
		foreach($translater as $table_translater_data)
		{
			foreach($table_translater_data as $translater_data)
			{
				if(stripos($translater_data['alias'],$alias)!==false)
				{
					$column_name=$translater_data['column'];
					break 2;
				}
			}
		}
	}
	return $column_name;
}
?>