<?php

include_once 'Table.php';
include_once 'Content.php';

class ContentTable extends Table{
	
	private $table_name = "website_content";
	private $translator = array(
			array("column"=>"txt_content","alias"=>"content"),
			array("column"=>"b_hidden","alias"=>"hidden"),
			array("column"=>"l_position","alias"=>"position"),
			array("column"=>"ref_a13f4053","alias"=>"page")
	);
	
	public function __construct()
	{
		parent::__construct($this->table_name,$this->translator);
	}
	
	public function getEntries($filter,$parsed=false)
	{
		$entries = parent::getEntries($filter);
		$contents=array();
		foreach($entries as $entry)
		{
			$contents[] = new Content($entry);
		}
		return $contents;
	}
}
?>