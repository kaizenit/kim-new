<?php
class Table{
	private $table_name = "";
	
	protected $special_query = NULL;
	
	private $translator = array(
			array("column"=>"lid","alias"=>"lid"),
			array("column"=>"luserid","alias"=>"luserid"),
			array("column"=>"luseridinsert","alias"=>"luseridinsert"),
			array("column"=>"dtedit::date","alias"=>"last_modified"),
			array("column"=>"dtinsert::date","alias"=>"inserted_at"),
	);
	
	protected function __construct($table_name,$extended_translator)
	{
		$this->table_name=$table_name;
		foreach($this->translator as &$column_alias_entries)
			$column_alias_entries['column']="{$this->table_name}.{$column_alias_entries['column']}";
		$this->translator = array_merge($this->translator,$extended_translator);
	}
	
	private function getColumnFromAlias($alias)
	{
		$column_name = null;
		foreach($this->translator as $table_translater_data)
		{
			if(strtolower($table_translater_data['alias'])==strtolower(trim($alias)))
			{
				$column_name=$table_translater_data['column'];
				break;
			}
		}
		if(!isset($column_name))
		{
			foreach($this->translator as $table_translater_data)
			{
				if(stripos($table_translater_data['alias'],$alias)!==false)
				{
					$column_name=$table_translater_data['column'];
					break;
				}
			}
		}
		return $column_name;
	}
	
	public function getEntries($filter)
	{
		global $dbhandle;
		$entries = array();
		$query = "SELECT ";
		foreach((array)$this->translator as $columns_aliases)
		{
			$query.=" {$columns_aliases['column']} as {$columns_aliases['alias']},
			";
		}
		$query.=isset($this->special_query)?$this->special_query:"
				$extra_query
				true
  				FROM {$this->table_name}
		";
		$query.="WHERE true";
		if(!empty($filter))
		{
			foreach((array)$filter as $alias=>$filter_values) //for each key we pass an array otherwise you couldn't do multiple filters on the same column/key
			{
				if(!is_array($filter_values))
					$filter_values=array($filter_values);
				foreach($filter_values as $filter_value)
				{
					$column_from_alias = $this->getColumnFromAlias($alias);
					if(isset($column_from_alias))
						$query.=" AND $column_from_alias $filter_value";
				}
						
			}
		}
		if(!empty($filter) && !empty($filter['special_query'])) // AND extract(month FROM date) =
			$query.=" {$filter['special_query']}";
		$query = "SELECT distinct on(lid) * FROM ($query)temp";
		if(!empty($filter) && !empty($filter['post_query']))
			$query.=" {$filter['post_query']}";
		$result = pg_query($dbhandle,$query);
		$entries = pg_fetch_all($result);
		if(!empty($entries))
		{
			if($filter['db_ready'])
			{
				foreach($entries as &$entry)
				{
					foreach($entry as $key=>&$value)
					{
						if(empty($value))
						{
							$value="NULL";
						}
						else
							$value="'".pg_escape_string($value)."'";
					}
				}
			}
		}
		else
			$entries = array();
		return $entries;
	}
}
?>