<?php
include_once 'Table.php';
class ProjectsTable extends Table{
	protected  $table_name = "projects";
	public $translator = array(
			array("column"=>"str_currency_f5003e92","alias"=>"currency"),
			array("column"=>"STR_PROJECTCODE_5CE8FC74","alias"=>"project_code"),
			array("column"=>"str_kicountry_2201e39b","alias"=>"country"),
			array("column"=>"flt_expensesperday_6a0b0159","alias"=>"expenses_per_day"),
			array("column"=>"str_soldby_7f469c28","alias"=>"sold_by_1"),
			array("column"=>"str_soldby2_8b82394a","alias"=>"sold_by_2"),
			array("column"=>"flt_sold1percentage_6b4639cb","alias"=>"sold_percentage_1"),
			array("column"=>"flt_sold2percentage_d9472e34","alias"=>"sold_percentage_2"),
			array("column"=>"str_soldby1country_cce402bb","alias"=>"sold_by_country_1"),
			array("column"=>"str_soldby2country_b15b7ea3","alias"=>"sold_by_country_2"),
			array("column"=>"ref_071edd0d","alias"=>"category_lid"),
			array("column"=>"b_fixedpriceproject_749f8160","alias"=>"is_fixed_price"),
			array("column"=>"ref_15cf77be","alias"=>"company_lid"),
			array("column"=>"FLT_DAYSOFFERED_FDD89E8A","alias"=>"days_offered"),
			array("column"=>"FLT_DAYRATE_30F23B59","alias"=>"day_rate"),
			array("column"=>"STR_INVOICECICLE","alias"=>"invoice_cycle"),
			array("column"=>"str_account_manager","alias"=>"account_manager"),
			array("column"=>"str_accountmanagercountry","alias"=>"account_manager_country"),
			array("column"=>"flt_accountmanagerperc","alias"=>"account_manager_perc")
	);
	public function __construct()
	{
		parent::__construct($this->table_name, $this->translator);
	}
}