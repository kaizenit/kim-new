<?php
include_once 'Data.php';

class Content extends Data
{
	protected $css = array();
	protected $js = array();
	public static $modules = array(); 
	
	public static function loadModules()
	{
		if(empty(self::$modules))
		{
			$files = scandir("modules");
			array_splice($files,0,2);
			foreach($files as $file)
			{
				$dotPos = strripos($file, ".");
				$fileName = substr($file, 0,$dotPos);
				self::$modules[]=$fileName;
			}
		}
	}
	
	private function parse()
	{
		if(empty(self::$modules))
			self::loadModules();
		$ordered_modules = array();
		$module_position_in_content=0;
		foreach(self::$modules as $module)
		{
			do
			{
				$module_position_in_content = stripos($this->content,"<$module>",$module_position_in_content);
				if($module_position_in_content!==false)
				{
					$ordered_modules[$module_position_in_content]=array(
							"module"=>$module,
					);
					$module_position_in_content+=strlen("<$module>");
				}
			}
			while ($module_position_in_content!==false);
			krsort($ordered_modules);
			foreach($ordered_modules as $position=>$ordered_module)
			{
				$end_position = stripos($this->content,"</{$ordered_module['module']}>",$position);
				if($end_position===false)
					$end_position=strlen($this->content);//just a fallback, shouldn't happen
				$after_module_opening = $position+strlen("<{$ordered_module['module']}>");
				$module_content = substr($this->content, $after_module_opening,$end_position-$after_module_opening);
				include_once ("modules".DIRECTORY_SEPARATOR."{$ordered_module['module']}.php");
				$render_function = "render_{$ordered_module['module']}";
				$assets_function = "getAssets_{$ordered_module['module']}";
				$assets = $assets_function();
				$rendered_html = $render_function($module_content);
				$this->content = substr_replace($this->content, $rendered_html, $position,$end_position+strlen("</{$ordered_module['module']}>")-$position);
				$this->css[$ordered_module['module']]=$assets['css'];
				$this->js[$ordered_module['module']]=$assets['js'];
			}
		}
	}
	public function __construct($data)
	{
		parent::__construct($data);
		$this->parse();
	}
}

?>