<?php

include_once 'Table.php';

class PageTable extends Table{

	private $table_name = "website_pages";
	private $translator = array(
			array("column"=>"str_alias","alias"=>"alias"),
			array("column"=>"str_country","alias"=>"country"),
			array("column"=>"txt_description","alias"=>"description"),
			array("column"=>"b_hidden","alias"=>"hidden"),
			array("column"=>"str_metatitle","alias"=>"meta_title"),
			array("column"=>"str_pagetitle","alias"=>"menu_title"),
			array("column"=>"l_position","alias"=>"position"),
			array("column"=>"ref_d38fbecf","alias"=>"parent_page"),
			array("column"=>"B_NOT_STANDARD","alias"=>"not_standard"),
			array("column"=>"str_shortcut","alias"=>"shortcut"),
			array("column"=>"b_kim","alias"=>"kim")
	);

	public function __construct()
	{
		parent::__construct($this->table_name,$this->translator);
	}
	
	public function getRecorsivePages($lang,$page_id,&$page=NULL)
	{
		$filter = array("country"=>" in ('all','$lang') ","parent_page"=>" IS NULL ","special_query"=>" ORDER BY position,lid ",
				"kim"=>" is true"
		);
		if(isset($page))
			$filter['parent_page']=" = {$page['lid']}";
		$pages = $this->getEntries($filter);
		foreach($pages as $key=>&$page_entry)
		{
			if(!isset($page_entry['children']))
				$page_entry['children']=array();
			if(!isset($page_entry['shortcut'])) //when the page is a simple shortcut we don't print anything below
				$page_entry['children']=$this->getRecorsivePages($lang,$page_id,$page_entry);
		}
		return $pages;
	}
	
	public function getPageIdFromAlias($alias_array,$lang="all",$parent_page_id=-1,$level=0,$depth_info=null)
	{
		if(!isset($depth_info))
			$depth_info=array("level"=>$level,"page_id"=>-1);
		if($level<count($alias_array) && isset($alias_array[$level]))
		{
			$filter = array("country"=>" = '$lang'");
			$filter['alias']=" = '".pg_escape_string($alias_array[$level])."'";
			if($parent_page_id>0)
				$filter['parent_page']="= $parent_page_id";//it's the parent page of the previous/children page
			$pages = $this->getEntries($filter);
			if(!empty($pages))
			{
				foreach($pages as $page)
				{
					if($level==count($alias_array)-1)//last level
					{
						return array("level"=>$level,"page_id"=>$page['lid']);
					}
					else //there are still levels
					{
						$depth_info_holder = $this->getPageIdFromAlias($alias_array,$lang,$page['lid'],$level+1,$depth_info);
						if($depth_info_holder['level']>=$depth_info['level'])
							$depth_info=$depth_info_holder;
					}
				}
			}
		}
		return $depth_info;
	}
}
?>