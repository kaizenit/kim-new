<?php
include_once 'Table.php';
include_once 'ProjectsTable.php';

class DaysTable extends Table
{
	private $table_name = "days";
	private $translator = array(
			array("column"=>"dt_date_4da7192d::date","alias"=>"day_date"),
			array("column"=>"str_projectcode_1edab67a","alias"=>"project_code"),
			array("column"=>"str_consultant_065a1337","alias"=>"consultant"),
			array("column"=>"str_comment_6a077ffd","alias"=>"comment"),
			array("column"=>"str_status_38f452a0","alias"=>"status"),
			array("column"=>"str_country_0ba53905","alias"=>"country"),
			array("column"=>"changekey","alias"=>"changekey"),
			array("column"=>"str_counttocountry_d3874f83","alias"=>"count_to_country"),
			array("column"=>"b_invoiced_d146bf3a","alias"=>"is_invoiced"),
			array("column"=>"l_type","alias"=>"l_type"),
			array("column"=>"ref_93e27796","alias"=>"workshop_lid"),
			array("column"=>"ref_a8b1e944","alias"=>"invoice_lid"),
			array("column"=>"flt_expense_cost","alias"=>"expense_cost"),
			array("column"=>"flt_mileage","alias"=>"mileage"),
			array("column"=>"str_route","alias"=>"route"),
			array("column"=>"str_transporttype","alias"=>"transport_type"),
			array("column"=>"str_currency","alias"=>"expense_currency"),
			array("column"=>"b_expense_invoiced","alias"=>"is_expense_invoiced"),
			array("column"=>"str_maintool","alias"=>"main_tool"),
			array("column"=>"ref_3279b082","alias"=>"day_product_lid")
	);
	protected $special_query = "
			(SELECT strlogin FROM vbluser WHERE lid = days.luserid limit 1) as changed_by,
  		(SELECT strlogin FROM vbluser WHERE lid = days.luseridinsert limit 1) as inserted_by,
  		cast(replace(str_nodays_06c3897f, ',','.') as float) as n_days,
  		CASE WHEN lower(str_name_0a24610c)='booked' THEN 'Invoiceable' ELSE CASE WHEN lower(str_name_0a24610c)='not invoiceable' THEN 'For Free' ELSE str_name_0a24610c END END as str_type,
  		(SELECT STR_INVOICENO_24F6EADA FROM XDATAGROUP453E0D78 WHERE lid = days.ref_a8b1e944 limit 1) as invoice_number,
  		projects.FLT_DAYRATE_30F23B59 as day_rate,
		special_rates.FLT_PRICE_73C5A946 as special_rate,
		 CASE
            WHEN str_projectcode_1edab67a::text = 'TBIN-001-POR'::text 
				THEN '#ff3300'::character varying(255)
            ELSE 
            	CASE WHEN (str_status_2aafe888::text = '4 Contract'::text 
						OR str_status_2aafe888::text = '7 Completed'::text 
						OR str_status_2aafe888 IS NULL OR (days.l_type = ANY (ARRAY[11, 9, 2]))) 
						AND days.str_status_38f452a0::text <> 'Estimated'::text THEN 
							day_types.str_colorcode_db475e72
                ELSE '#FFFFFF'::character varying(255)
            END
        END AS color,
		CASE WHEN to_char(dt_date_4da7192d,'HH24:MI:SS AM')='00:01:00 AM' 
			THEN NULL 
			ELSE to_char(dt_date_4da7192d,'HH24:MI')
		END  as from_time,
		CASE WHEN dt_to IS NOT NULL THEN to_char(dt_to,'HH24:MI') ELSE NULL END as to_time
  		FROM XDATAGROUPD8B41609 days
  		INNER JOIN XTABLE423058F8 projects on days.str_projectcode_1edab67a = projects.STR_PROJECTCODE_5CE8FC74
  		LEFT JOIN XDATAGROUPD13D0714 special_rates ON special_rates.fklid = projects.lid AND special_rates.STR_CONSULTANT_21C18580 = days.str_consultant_065a1337  				   
  		LEFT JOIN xdatagroup3cf9152f day_types ON days.l_type=day_types.lid
			";
	public function __construct()
	{
		$project_table = new ProjectsTable;
		foreach($this->translator as &$column_alias_entries)
			$column_alias_entries['column']="{$this->table_name}.{$column_alias_entries['column']}";
		foreach($project_table->translator as $column_alias)
			$this->translator[]=array("column"=>"projects.{$column_alias['column']}","alias"=>"projects_{$column_alias['alias']}");
		parent::__construct($this->table_name, $this->translator);
	}
}