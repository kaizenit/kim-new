<?php
class Data
{
	private $data = array();
	
	public function __construct($data)
	{
		$this->data=$data;
	}
	
	public function __get($name)
	{
		$result = null;
		$result = $this->data[$name];
		if(!isset($result))
			$result = $this->$name;
		return $result;
	}
	
	public function __set($name,$value)
	{
		if(isset($this->data) && isset($this->data[$name]))
			$this->data[$name]=$value;
		else
			$this->$name=$value;
	}
}
?>