<?php

$pageTable = new PageTable;

$pageUrl = $_SERVER['REQUEST_URI'];
if(stripos($pageUrl,"?")>0) //cleaning GET params
	$pageUrl = substr($pageUrl,0,stripos($pageUrl,"?"));
	
if(stripos($pageUrl,".")>0)
{
	$urlParams = preg_split('/[\/.]/',$pageUrl);
	array_pop($urlParams);
}
else
	$urlParams = preg_split('/[\/]/',$pageUrl);

array_splice($urlParams,0,2);
if(strlen(trim($urlParams[0]))==0)
	array_splice($urlParams,0);

//urlParams has the base url juice
$baseFixUrl = $urlParams;
if(count($urlParams)>0 && $urlParams[0]!="index" && $urlParams[0]!="home" && $urlParams[0]!="m" && strlen($urlParams[0])>0)
{
	$depth_info=$pageTable->getPageIdFromAlias($urlParams,$lang);
	$page = $depth_info['page_id'];
	array_splice($urlParams, 0,$depth_info['level']+1);
	if($page<0)
		$is404=true;
	
}
else
{
	if($urlParams[0]=="m")
		$is404=true;//for the module to decide although it's the first page
	$baseUrl[]=$urlParams[0];
	$page=1;
}

?>
