<?php 
include_once 'orm/PageTable.php';
include_once 'orm/ContentTable.php';

$myServer = "192.168.101.24";
$myUser = "intrexx";
$myPass = "intrexx";
$myDB = "ixkimglobal";
$dbhandle = pg_connect("host=$myServer port=5432 dbname=$myDB user=$myUser password=$myPass connect_timeout=10");
$user = 'lazevedo';
$lang='gl';
//find page ID
$base_url = array();
$is404 = false;
if (! empty ( $_GET ["page"] ))
	$page = $_GET ["page"]; // adding the ?page=X will overwrite the url behaviour
if (! isset ( $page ))
	include_once ('urlHandler.php');
$page_table = new PageTable;
$current_page_filter = array("lid"=>"=$page");
$current_page = $page_table->getEntries($current_page_filter);
$current_page=$current_page[0];
//fetch content
$content_table = new ContentTable;
$content_filter = array("page"=>" = $page ");
$contents = $content_table->getEntries($content_filter);
$json_request=isset($_REQUEST['isAjax']);
//process content
?>
<?php if(!$json_request):?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= $current_page['meta_title'];?></title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet" type="text/css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <?php 
    foreach($contents as $content):
    	foreach($content->css as $styles):
    		foreach($styles as $css):?>
   			<link rel="stylesheet" type="text/css" href="<?= $css?>" media="all">
   			<?php endforeach;?>
   		<?php endforeach;?>
    <?php endforeach;?>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
		integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
		crossorigin="anonymous"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<?php include_once 'menu.php';?>
		<div class="row">
			<div class="col-xs-12 grey-row" style="padding-bottom:10px">
				<h1 class="text-center">Welcome to Kaizen Information System (KIM)</h1>
				<p class="text-center" style="padding-bottom:10px;">KIM is the the intranet system of Kaizen Institute.<br/>
					It provides full management services to KI's BUs.<br/>
					If you need support, please write an email to <a href="mailto:support@kaizen.com">support@kaizen.com</a>.</p>
			</div>
		</div>
<?php endif;?>
		<?php foreach($contents as $content):?>
		<?= $content->content;?>
		<?php endforeach;?>
<?php if(!$json_request):?>
	</div>
	<?php 
    foreach($contents as $content):
    	foreach($content->js as $scripts):
    		foreach($scripts as $js):?>
    		<script type="text/javascript" src="<?= $js?>"></script>
   			<?php endforeach;?>
   		<?php endforeach;?>
    <?php endforeach;?>
</body>
</html>
<?php endif;?>