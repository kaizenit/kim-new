var gulp = require('gulp');
var gulpLoadPlugins = require( 'gulp-load-plugins');
var sass = require('gulp-sass');
const $ = gulpLoadPlugins();
gulp.task('sass', function() {
  return gulp.src('scss/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe($.sass({
      precision: 10
    }).on('error', $.sass.logError))
    .pipe(gulp.dest('css'))
})

gulp.task('watch',function(){
	gulp.watch('scss/**/*.scss', ['sass']);
})
 