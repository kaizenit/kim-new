<?php
include_once 'orm/DaysTable.php';

function getConsultantDaysTypeProjectsDistribution($start,$end,$consultants)
{
	$start = date("Y-m-d", strtotime($start));
	$end = date("Y-m-d", strtotime($end));
	$days_by_type = array();
	$days_by_project = array();
	$days = getDaysEvents($start,$end,$consultants);
	foreach ($days as $day_date=>$entries)
	{
		foreach($entries as $entry)
		{
			$days_by_type[$entry['str_type']][]=$entry;
			$days_by_type[$entry['str_type']]['total_days']+=$entry['n_days'];
			$days_by_type[$entry['str_type']]['color']=$entry['color'];
			$days_by_project[$entry['project_code']][]=$entry;
			$days_by_project[$entry['project_code']]['total_days']+=$entry['n_days'];
		}
	}
	return array("days_by_type"=>$days_by_type,"days_by_project"=>$days_by_project);
}

function getWeeks($year,$month,$consultants)
{
	$start_day = date('w',strtotime("{$year}-{$month}-01"))==1?"{$year}-{$month}-01":date('Y-m-d', strtotime('previous Monday', strtotime("{$year}-{$month}-01")));
	$day_iterator = date_create_from_format("Y-m-d", $start_day);
	$last_day = date_create_from_format("Y-m-d", date('Y-m-t',strtotime("{$year}-{$month}-01")));
	if($last_day->format('w')!=0)//if doesn't end on a Sunday
		$last_day=date_create_from_format('Y-m-d',date("Y-m-d",strtotime('next Sunday', strtotime($last_day->format("Y-m-d")))));

	$weeks = array();
	$days_events = getDaysEvents($day_iterator->format('Y-m-d'), $last_day->format('Y-m-d'), $consultants);
	while($day_iterator<=$last_day)
	{
		$week_number = date_format($day_iterator, "W");
		$belongs_to_month = $day_iterator->format('m')==$month;
		$is_today = $day_iterator->format('Y-m-d')==date('Y-m-d');
		$is_weekend = in_array($day_iterator->format('w'),array("0","6"));
		$day_events = $days_events[$day_iterator->format('Y-m-d')];
		if(!isset($day_events))
			$day_events=array();
			$weeks[$week_number][]=array(
					"date"=>clone $day_iterator,
					"belongs_to_month"=>$belongs_to_month,
					"is_today"=>$is_today,
					"is_weekend"=>$is_weekend,
					"day_events"=>$day_events
			);
			date_add($day_iterator, new DateInterval('P1D'));
	}
	return $weeks;

}

function getUsersByCountry()
{
	global $dbhandle;
	$query = "SELECT * FROM users_country_view ";
	$query.=" order by country,username";
	$result = pg_query($dbhandle,$query);
	$users_by_countries = array("long_country"=>array(),"small_country"=>array());
	for($i=0;$i<pg_num_rows($result);$i++)
	{
		$ar_Row = pg_fetch_assoc($result);
		$users_by_countries['small_country'][$ar_Row['country_lc']][]=$ar_Row;
		$users_by_countries['long_country'][$ar_Row['country']][]=$ar_Row;
	}
	return $users_by_countries;
}

function getDaysEvents($start,$end,$consultants)
{
	$days_table = new DaysTable;
	// Get Planner Entries for consultants
	$consultants_string = "('".implode("','", $consultants)."')";
	$filter=array("consultant"=>" IN $consultants_string"
			,"day_date"=>" >='$start' and dt_date_4da7192d<='$end'","special_query"=>" order by day_date asc, from_time asc");
	$days = $days_table->getEntries($filter);
	$index_days = array();
	foreach($days as $day)
		$index_days[$day['day_date']][]=$day;
	return $index_days;
}

function getEventsFeed()
{
	global $dbhandle;
	$events = array("college"=>array(),"others"=>array());
	$query = "SELECT * FROM (
			(SELECT * FROM events_feed where seminar_start_date>=now()::date and category='College'  order by seminar_start_date limit 5) UNION
			(SELECT * FROM events_feed where seminar_start_date>=now()::date and category!='College' order by seminar_start_date) )before_order
			order by seminar_start_date";
	$result = pg_query($dbhandle,$query);
	for($i=0;$i<pg_num_rows($result);$i++)
	{
		$ar_Row = pg_fetch_array($result);
		if(strtolower($ar_Row['category'])=='college')
			$events['college'][]=$ar_Row;
			else $events['others'][]=$ar_Row;
	}
	return $events;
}

function getAssets_calendar()
{
	return array(
			"css"=>array(
					"https://kim.kaizen.com/new/css/calendar.css",
					"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
			),
			"js"=>array(
					"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js",
					"https://www.gstatic.com/charts/loader.js",
					"https://kim.kaizen.com/new/js/calendar.js"
			)
	);
}

function render_calendar($module_content)
{
	/*
	 * variables
	 */
	global $user;
	$isAjax=isset($_REQUEST['isAjax']);
	$year = isset($_REQUEST['year'])?$_REQUEST['year']:date("Y");
	$month = isset($_REQUEST['month'])?$_REQUEST['month']:date("m");
	$consultants = isset($_REQUEST['consultants'])?$_REQUEST['consultants']:array($user);
	$week_days = array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");
	$first_of_year = date('01.01.Y');
	$end_of_month = date('t.m.Y');
	$users_by_country = getUsersByCountry();
	$events = getEventsFeed();
	$weeks = getWeeks($year,$month,$consultants);
	
	if(isset($_REQUEST['next_prev']))
	{
		$date_from_year_month = date_create_from_format("Y-m-d", "{$year}-{$month}-1");
		if($_REQUEST['next_prev']=='next')
		{
			$next_date = $date_from_year_month->add(new DateInterval('P1M'));
			$year = $next_date->format('Y');
			$month = $next_date->format('m');
		}
		elseif($_REQUEST['next_prev']=='prev')
		{
			$prev_date = $date_from_year_month->sub(new DateInterval('P1M'));
			$year = $prev_date->format('Y');
			$month = $prev_date->format('m');
		}
	}
	
	if(isset($_REQUEST['chart_type']))
	{
		header('Content-Type: application/json');
		$days = getConsultantDaysTypeProjectsDistribution($_REQUEST['start_date'],$_REQUEST['end_date'],$consultants);
		echo json_encode($days);
		die;
	}
	
	ob_start();
?>
<?php if(!$isAjax):?>
<div class="row">
	<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12">
						<div class="row" id="outter-calendar">
<?php endif;?>
						<div class="col-xs-12 calendar-container">
							<div class="row" style="margin-bottom:10px">
								<div class="col-xs-12 text-center">
									<h3 style="display:inline-block;font-size:30px" class="pull-left">Planner</h3>
									<span onclick="loadMonth(<?php echo $year?>,<?php echo $month?>,'prev');"
										class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<h4 class="month">
										<?php echo DateTime::createFromFormat('!m', $month)->format("F")." $year";?>
									</h4>
									<span
										onclick="loadMonth(<?php echo $year?>,<?php echo $month?>,'next');"
										class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span id="refreshMonth"
										onclick="loadMonth(<?php echo $year?>,<?php echo $month?>,'');"
										class="glyphicon glyphicon-refresh pull-right" aria-hidden="true"></span>
									<select id="consultant-selector" onchange="loadMonth(<?php echo $year?>,<?php echo $month?>,'')">
									<?php foreach($users_by_country['long_country'] as $user_country=>$user_infos):?>
									<optgroup label="<?php echo $user_country;?>">
										<?php foreach($user_infos as $user_info):?>
											<option <?php echo $user_info['username']==$consultants[0]?"selected":"" ?>><?php echo $user_info['username'];?></option>
										<?php endforeach;?>
									</optgroup>
									<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="row week-days-row">
								<?php foreach($week_days as $week_day):?>
									<div class="col-xs-12 text-center week-days" style="width: 14.28%"><?php echo substr($week_day, 0,3)."<span class='hidden-xs'>".substr($week_day, 3)."</span>" ;?></div>
								<?php endforeach;?>
							</div>
							<?php foreach($weeks as $week_number=>$days_array):?>
							<div class="row days-row">
								<?php foreach($days_array as $day_info):
								$mobile_info=array();
								?>
								<div
									class="col-xs-12 day text-center <?php echo $day_info['is_today']?"today":""; echo $day_info['is_weekend']?"weekend":"";?>"
									onclick="showDayModal(this,event);" data-day-consultant="<?php echo $consultants[0];?>" data-day-date="<?php echo $day_info['date']->format('d.m.Y');?>" >
									<span
										class="day-number text-left <?php echo !$day_info['belongs_to_month']?"not-in-month":""; ?> ">
										<?php echo $day_info['date']->format('d');?>
									</span>
									<div style="position: relative">
										<div class="days-events">
											<?php foreach($day_info['day_events'] as $day_event):
											$mobile_info[]=array("project_code"=>$day_event['project_code'],"background_color"=>$day_event['color'],"n_days"=>$day_event['n_days'],"day_id"=>$day_event['lid'],"consultant"=>$day_event['consultant'],"date"=>$day_info['date']->format('d.m.Y'),
													"to_time"=>$day_event['to_time'],"from_time"=>$day_event['from_time']
											); 
											?>
											<div onclick="showDayModal(this,event);" class="event-entry hidden-xs hidden-sm" data-day-id="<?php echo $day_event['lid'];?>" data-day-project-code="<?php echo $day_event['project_code'];?>" data-day-consultant="<?php echo $day_event['consultant'];?>" data-day-date="<?php echo $day_info['date']->format('d.m.Y');?>" style="background-color:<?php echo $day_event['color'];?>">
												<span class="project-code"><?php echo $day_event['project_code']?></span>
											</div>
											<?php endforeach;?>
											<?php if(!empty($mobile_info)):?>
											<div onclick="showMobileInfo(this,event);" class="visible-xs visible-sm mobile-day-info" style="background-color:#98F0FB" data-day-consultant="<?php echo $consultants[0];?>" data-day-date="<?php echo $day_info['date']->format('d.m.Y');?>" data-events-info='<?php echo json_encode($mobile_info);?>'></div>
											<?php else: ?>
											<span  class="visible-xs visible-sm glyphicon glyphicon-plus" aria-hidden="true"></span>
											<?php endif;?>
										</div>
									</div>
								</div>
								<?php endforeach;?>
							</div>
							<?php endforeach;?>
							<div class="visible-sm visible-xs row" style="margin-top: 10px;">
								<div class="col-xs-12 well" id="mobile-area-info"></div>
							</div>
							<div class="col-xs-12 text-center" id="overlay" style="display: none">
								<img alt="Loading..." src="https://kim.kaizen.com/planner/loading.gif"
									class="loading-image img-responsive" />
							</div>
						</div>
<?php if(!$isAjax):?>
				</div>
			</div>
		</div>
				<div class="row grey-row" style="margin-top:10px">
					<div class="col-xs-12">
						<h1 class="text-center">KPIs (Key Performance Indicators)</h1>
					</div>
					<div class="col-xs-12 col-sm-6 text-center chart-area">
						<h2>Consultant days per type</h2>
						<div class="form-group form-inline">
							<label>From:
								<input type="text" value="<?= $first_of_year?>" class="start_date form-control charts-control" placeholder="Start date">
							</label>
						</div>
						<div class="form-group form-inline">
							<label>To:
								<input type="text" value="<?= $end_of_month?>" class="end_date form-control charts-control" placeholder="End date">
							</label>
						</div>
						<div id="days_by_type" class="chart col-xs-12">
							<span class="refresh"><span class="glyphicon glyphicon-refresh spinning"></span> Loading...</span>
							<div class="chart-holder" style="width:100%">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 text-center chart-area">
						<h2>Consultant days per project</h2>
						<div class="form-group form-inline">
							<label>From:
								<input type="text" value="<?= $first_of_year?>" class="start_date form-control charts-control" placeholder="Start date">
							</label>
						</div>
						<div class="form-group form-inline">
							<label>To:
								<input type="text" value="<?= $end_of_month?>" class="end_date form-control charts-control" placeholder="End date">
							</label>
						</div>
						<div id="days_by_project" class="chart">
							<span class="refresh"><span class="glyphicon glyphicon-refresh spinning"></span> Loading...</span>
							<div class="chart-holder">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<img src="./lib/pictures/trainings.png" class="img-responsive" />
					</div>
				</div>
				<div class="row row-suggestion">
					<div class="col-xs-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 suggestion text-center">
						<h1 style="color:white">Suggestion box</h1>
						<div class="row">
							<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
								<textarea class="form-control" rows="6"></textarea>
							</div>
							<div class="col-xs-8 col-xs-offset-2 col-md-offset-0 col-md-2 col-lg-2">
								<button class="btn btn-default">Submit</button>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="row" style="padding-top:10px">
							<div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
								<img src="./lib/pictures/footer.svg" class="img-responsive" />
							</div>
						</div>
					</div>
				</div>
			</div>
</div>
<div class="modal fade" id="day-modal" tabindex="-1" role="dialog"
	aria-labelledby="day-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button id="close-model" type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="day-modal-title">&nbsp;</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12" style="overflow-x: hidden;">
						<iframe id="day-form"></iframe>
					</div>
				</div>
				<div class="text-center" id="modal-overlay"
					style="display: none;">
					<img alt="Loading..." src="loading.gif"
						class="loading-image img-responsive" />
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
endif;
return ob_get_clean();
}?>